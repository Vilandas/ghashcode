import java.util.ArrayList;

public class Data
{
    private int numOfBooks;
    private int numOfLibraries;
    private int daysForScanning;

    private ArrayList<Book> books;
    private ArrayList<Library> libraries;

    public Data(int numOfBooks, int numOfLibraries, int daysForScanning, ArrayList<Book> books, ArrayList<Library> libraries)
    {
        this.numOfBooks = numOfBooks;
        this.numOfLibraries = numOfLibraries;
        this.daysForScanning = daysForScanning;
        this.books = books;
        this.libraries = libraries;
    }

    public int getNumOfBooks()
    {
        return numOfBooks;
    }

    public int getNumOfLibraries()
    {
        return numOfLibraries;
    }

    public int getDaysForScanning()
    {
        return daysForScanning;
    }

    public ArrayList<Book> getBooks()
    {
        return books;
    }

    public ArrayList<Library> getLibraries()
    {
        return libraries;
    }

    @Override
    public String toString()
    {
        return "Data{" +
                "numOfBooks=" + numOfBooks +
                ", numOfLibraries=" + numOfLibraries +
                ", daysForScanning=" + daysForScanning +
                ", books=" + books +
                ", libraries=" + libraries +
                '}';
    }
}

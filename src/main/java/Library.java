import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Library implements Comparable<Library> {
    private int id;
    private int signupTime;
    private int booksPerDay;
    private ArrayList<Book> books;
    private int daysAvailable;
    private int scannedBooks;

    public Library(int id, int signupTime, int booksPerDay, ArrayList<Book> books, int daysAvailable) {
        this.id = id;
        this.signupTime = signupTime;
        this.booksPerDay = booksPerDay;
        this.books = books;
        Collections.sort(books);
        this.daysAvailable = daysAvailable;
        this.scannedBooks = 0;
    }

    public Library(Library library) {
        this.id = library.getId();
        this.signupTime = library.getSignupTime();
        this.booksPerDay = library.getBooksPerDay();
        this.books = library.getBooks();
        this.daysAvailable = library.daysAvailable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSignupTime() {
        return signupTime;
    }

    public void setSignupTime(int signupTime) {
        this.signupTime = signupTime;
    }

    public int getBooksPerDay() {
        return booksPerDay;
    }

    public void setBooksPerDay(int booksPerDay) {
        this.booksPerDay = booksPerDay;
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public ArrayList<Book> getScannableBooks() {
        int daysUsable = this.daysAvailable - this.signupTime;
        int numBooksToSend = this.booksPerDay * daysUsable;
        ArrayList<Book> booksToSend = (ArrayList<Book>) this.books.subList(0, numBooksToSend);
        return booksToSend;
    }

    public List<Book> getScannableBooks(int dayStart) {
        int daysUsable = this.daysAvailable - this.signupTime - dayStart;
        int numBooksToSend = this.booksPerDay * daysUsable;
        if(numBooksToSend > this.books.size()) {
            numBooksToSend = this.books.size();
        }
        List<Book> booksToSend = this.books.subList(0, numBooksToSend);
        return booksToSend;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

    public void removeBook(Book book) {
        this.books.remove(book);
        Collections.sort(this.books);
    }

    public int getDaysAvailable() {
        return daysAvailable;
    }

    public void setDaysAvailable(int daysAvailable) {
        this.daysAvailable = daysAvailable;
    }

    public Book getNextBook() {
        if(this.scannedBooks >= this.books.size()) {
            return null;
        }
        Book b = this.books.get(this.scannedBooks);
        this.scannedBooks++;
        return b;
    }
    
    public int sumOfBooks(List<Book> books) {
        int count = 0;
        for (Book book :
                books) {
            count += book.getScore();
        }
        return count;
    }

    public int getMaxScore() {
        int daysUsable = this.daysAvailable - this.signupTime;
        int numBooksToSend = this.booksPerDay * daysUsable;
        if(numBooksToSend > this.books.size()) { numBooksToSend = this.books.size(); }
        if(numBooksToSend < 0) {
            numBooksToSend = 0;
        }
        List<Book> booksToSend = this.books.subList(0, numBooksToSend);
        return sumOfBooks(booksToSend);
    }

    @Override
    public int compareTo(Library o) {
        return this.getMaxScore() - o.getMaxScore();
    }

    @Override
    public String toString()
    {
        return "Library{" +
                "id=" + id +
                ", signupTime=" + signupTime +
                ", booksPerDay=" + booksPerDay +
                ", books=" + books +
                ", daysAvailable=" + daysAvailable +
                '}';
    }
}

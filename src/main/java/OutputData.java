import java.util.ArrayList;

public class OutputData
{
    private int librariesForScanning;
    private int[] libraries;
    private int[] booksForScanning;
    private int[][] bookOrder;

    public static String getOutput(ArrayList<Library> libraries) {
        String output = "";
        int day = 0;
        output += libraries.size() + "\n";
        for (Library lib :
                libraries) {
            output += lib.getId() + " " + lib.getScannableBooks(day).size() + "\n";
            for (Book book :
                    lib.getScannableBooks(day)) {
                output += book.getId() + " ";
            }
            output.substring(0, output.length() - 1);
            output += "\n";
            day += lib.getSignupTime();
        }
        return output;
    }

    public OutputData(int librariesForScanning, int[] libraries, int[] booksForScanning, int[][] bookOrder)
    {
        this.librariesForScanning = librariesForScanning;
        this.libraries = libraries;
        this.booksForScanning = booksForScanning;
        this.bookOrder = bookOrder;
    }

    public int getLibrariesForScanning()
    {
        return librariesForScanning;
    }

    public int[] getLibraries()
    {
        return libraries;
    }

    public int[] getBooksForScanning()
    {
        return booksForScanning;
    }

    public int[][] getBookOrder()
    {
        return bookOrder;
    }
}

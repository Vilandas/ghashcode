import java.util.Objects;

public class Book implements Comparable
{
    int id;
    int score;

    public Book(int id, int score)
    {
        this.id = id;
        this.score = score;
    }

    public int getId()
    {
        return id;
    }

    public int getScore()
    {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return getId() == book.getId() &&
                getScore() == book.getScore();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getScore());
    }

    @Override
    public int compareTo(Object book)
    {
        return (((Book)(book)).score) - this.score;
    }

    @Override
    public String toString()
    {
        return "Book{" +
                "id=" + id +
                ", score=" + score +
                '}';
    }
}

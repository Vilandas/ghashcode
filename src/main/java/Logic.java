import java.util.ArrayList;
import java.util.Collections;

public class Logic
{

    public void start()
    {
        LoadData ld = new LoadData();
        //String file = "a_example.txt";
        //String file = "b_read_on.txt";
        //String file = "c_incunabula.txt";
        String file = "d_tough_choices.txt";
        //String file = "e_so_many_books.txt";
        //String file = "f_libraries_of_the_world.txt";
        ld.readFile(file);
        Data data = ld.data;

        int daysAvailable = data.getDaysForScanning();
        ArrayList<Library> libraries = data.getLibraries();
        Collections.sort(libraries);

        ArrayList<Book> scannedBooks = new ArrayList<>();

        Library signupLibrary = libraries.get(0);
        ArrayList<Library> scanningLibraries = new ArrayList<>();

        int day = 0;
        int nextScanDay = signupLibrary.getSignupTime();
        while(day < daysAvailable) {
            if(day % 1000 == 0)
            {
                System.out.println("DAY " + day);
            }
            if(day >= nextScanDay && libraries.size() > 0) {
                if(nextScanDay == 0) {
                    scanningLibraries.add(new Library(signupLibrary));
                    //System.out.println("ADDED LIB "+signupLibrary.getId());
                    libraries.remove(0);
                    if(libraries.size() > 0) {
                        signupLibrary = libraries.get(0);
                        nextScanDay = signupLibrary.getSignupTime();
                    }

                }
            }
            for (Library library : scanningLibraries) {
                for (int i = 0; i < library.getBooksPerDay(); i++) {
                    Book bookToScan = library.getNextBook();
                    while(scannedBooks.contains(bookToScan)) {
                        library.removeBook(bookToScan);
                        bookToScan = library.getNextBook();
                    }
                    if(bookToScan != null) {
                       // System.out.println("SCANNING BOOK "+bookToScan.getId()+" FROM LIB"+library.getId());
                        scannedBooks.add(bookToScan);
                        for (Library lib : libraries) {
                            lib.removeBook(bookToScan);
                        }
                    }
                }
                Collections.sort(libraries);
            }
            day++;
            nextScanDay--;
        }

        System.out.println(scanningLibraries);
        System.out.println(scannedBooks);
        System.out.println();
        System.out.println(OutputData.getOutput(scanningLibraries));

        WriteData wd = new WriteData();
        wd.writeFile(OutputData.getOutput(scanningLibraries), file);
    }
}

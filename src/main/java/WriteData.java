import java.io.FileWriter;
import java.io.IOException;

public class WriteData
{
    public void writeFile(String data, String filename)
    {
        filename = filename.substring(0, filename.indexOf("."));
        try(FileWriter outputFile = new FileWriter(filename + "_output.txt"))
        {
            outputFile.write(data);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

}

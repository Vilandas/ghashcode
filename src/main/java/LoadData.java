import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class LoadData
{
    public Data data;

    public void readFile(String file)
    {
        try(Scanner in = new Scanner(new BufferedReader(new FileReader(file))))
        {
            String input = in.nextLine();
            String[] inputArray = input.split(" ");
            int noOfBooks = Integer.parseInt(inputArray[0]);
            int noOfLibraries = Integer.parseInt(inputArray[1]);
            int daysForScanning = Integer.parseInt(inputArray[2]);

            ArrayList<Book> books = new ArrayList();
            ArrayList<Library> libraries = new ArrayList();

            while(books.size() < noOfBooks)
            {
                input = in.nextLine();
                inputArray = input.split(" ");

                for (int j = 0; j < inputArray.length; j++)
                {
                    int value = Integer.parseInt(inputArray[j]);
                    books.add(new Book(j, value));
                }
            }

            int libraryId = 0;

            while (in.hasNextLine())
            {
                ArrayList<Book> libBooks = new ArrayList();
                input = in.nextLine();
                if(input.length() == 0)
                {
                    continue;
                }
                inputArray = input.split(" ");

                int signUpProcess = Integer.parseInt(inputArray[1]);
                int booksPerDay = Integer.parseInt(inputArray[2]);
                input = in.nextLine();
                inputArray = input.split(" ");

                for(int i = 0; i < inputArray.length; i++)
                {
                    int value = Integer.parseInt(inputArray[i]);
                    libBooks.add(new Book(books.get(value).id, books.get(value).score));
                }

                Library lib = new Library(libraryId, signUpProcess, booksPerDay, libBooks, daysForScanning);
                libraries.add(lib);
                libraryId++;
            }
            data = new Data(noOfBooks, noOfLibraries, daysForScanning, books, libraries);

        }
        catch(IOException e)
        {

        }
    }
}
